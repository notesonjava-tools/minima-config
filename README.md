# minima-config

Property Loader for java that reads environment variables, system properties and property files.

Priority order :
property file
System properties, including command line args
Environment, 

Example in docker-compose file

  	verbs-batch:
    	image: notesonjava/verbs-batch
    	container_name: verbs-batch 
    	external_links:
    	- verbs-db    
    	environment:
    	- mongo_host=verbs-db
  	verbs-db:
    	image: mongo
    	container_name: verbs-db
    	ports:
    	- "27017:27017"
    
The variable mongo_host will be added in memory as 'mongo.host' and can be accessed as an Optional<String> like :
		
		PropertyMap prop = PropertyLoader.loadProperties();
		String mongoPort = prop.get("mongo.port").orElse("27017");
		String mongoHost = prop.get("mongo.host").orElse("localhost");

When loading properties, you can add file name as a string.  The name must be the classpath location.
A file application.properties inside src/main/resources can be loaded like : 
	
		PropertyMap prop = PropertyLoader.loadProperties("application.properties");
    