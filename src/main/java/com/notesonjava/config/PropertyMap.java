package com.notesonjava.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

public class PropertyMap {

	private Map<String, String> map = new HashMap<>();
	
	public void put(String key, String value) {
		map.put(key, value);
	}
	
	public Optional<String> get(String key){
		if(map.containsKey(key)) {
			return Optional.of(map.get(key));
		}
		return Optional.empty();
	}
	
	public Set<Entry<String, String>> entrySet(){
		return map.entrySet();
	}

	public void clear() {
		this.map.clear();
	}
	
}
