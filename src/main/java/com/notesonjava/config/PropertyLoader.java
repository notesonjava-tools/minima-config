package com.notesonjava.config;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Slf4j
public class PropertyLoader {

	public static PropertyMap loadProperties(String...fileNames) throws IOException{
		PropertyMap result = new PropertyMap();
		Properties prop = new Properties();
		for(int i=0; i< fileNames.length; i++){
			String fileName = fileNames[i];
			try(InputStream input = PropertyLoader.class.getClassLoader().getResourceAsStream(fileName)){
				log.debug("Load property file: " + fileName);
				prop.load(input);
				log.debug("Found : " + prop.size());
				prop.entrySet().forEach(entry -> result.put((String)entry.getKey(), (String)entry.getValue()));

			}
        }

        if (log.isDebugEnabled()) {
            log.debug("------------- Properties loaded from file---------------- ");//property file
            result.entrySet().forEach(entry -> log.debug(entry.toString()));

        }
		
		log.debug("------------- Properties loaded from env---------------- ");// docker-env, do not accept '.' in property names
		System.getenv().entrySet().forEach(entry -> {
			log.debug(entry.toString());
            String key = entry.getKey().toLowerCase().replace("_", ".");
			log.debug("New key : " + key);
            result.put(key, entry.getValue());
		});

		log.debug("------------- Properties loaded from System.properties---------------- ");//command lines with -D
		System.getProperties().entrySet().forEach(entry -> {
			log.debug(entry.toString());
			String key = entry.getKey().toString().toLowerCase().replace("_", ".");
			result.put(key, entry.getValue().toString());
		});
		
		
        if (log.isDebugEnabled()) {
            log.debug("------------- Properties loaded---------------- ");
            result.entrySet().forEach(entry -> log.debug(entry.toString()));
        }
		return result;
	}

}
