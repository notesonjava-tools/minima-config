package com.notesonjava.config;

import java.io.IOException;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class PropertyLoaderTest {

	
	@Test
	public void loadSingleFromClassPath() throws IOException {
		PropertyMap props = PropertyLoader.loadProperties("test.properties");
		Optional<String> intValue = props.get("int.value");
		Assertions.assertThat(intValue.isPresent()).isTrue();
		Assertions.assertThat(intValue.get()).isEqualTo("2");
		
		Optional<String> stringValue = props.get("string.value");
		Assertions.assertThat(stringValue.isPresent()).isTrue();
		Assertions.assertThat(stringValue.get()).isEqualTo("value");
		
	}
	
	@Test
	public void loadMultipleFilesFromClassPath() throws IOException {
		PropertyMap props = PropertyLoader.loadProperties("test.properties", "test2.properties");
		Optional<String> intValue = props.get("int.value");
		Assertions.assertThat(intValue.isPresent()).isTrue();
		Assertions.assertThat(intValue.get()).isEqualTo("2");
		
		Optional<String> stringValue = props.get("string.value");
		Assertions.assertThat(stringValue.isPresent()).isTrue();
		Assertions.assertThat(stringValue.get()).isEqualTo("value");
		
		Optional<String> booleanValue = props.get("boolean.value");
		Assertions.assertThat(booleanValue.isPresent()).isTrue();
		Assertions.assertThat(booleanValue.get()).isEqualTo("true");
		
	}
	

	@Test
	public void loadWithoutFiles() throws IOException {
		PropertyMap props = PropertyLoader.loadProperties();
		props.entrySet().forEach(System.out::println);
		Optional<String> javaHome = props.get("java.home");
		Assertions.assertThat(javaHome.isPresent()).isTrue();
		Assertions.assertThat(javaHome.get()).isNotBlank();
		
	}
}
